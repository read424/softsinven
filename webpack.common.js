const path= require('path');
const CleanWebpackPlugin= require('clean-webpack-plugin');
const HtmlWebpackPlugin= require('html-webpack-plugin');
const webpack = require('webpack');

module.exports={
    entry:['./src/app/app.js','./src/app/custom.js'],
    plugins:[
        new CleanWebpackPlugin(['dist']),
        new HtmlWebpackPlugin({
            filename: 'index.html',
            template:path.resolve(__dirname, 'src/app/index.html')
        }),
        new webpack.ProvidePlugin({
            $:'jquery',
            jQuery: 'jquery',
            'windows.jQuery': 'jquery',
            Popper: ['popper.js', 'default'],
            'window.Tether': 'tether',
            Alert: 'exports-loader?Alert!bootstrap/js/dist/alert',
            Button: 'exports-loader?Button!bootstrap/js/dist/button',
            Carousel: 'exports-loader?Carousel!bootstrap/js/dist/carousel',
            Collapse: 'exports-loader?Collapse!bootstrap/js/dist/collapse',
            Dropdown: 'exports-loader?Dropdown!bootstrap/js/dist/dropdown',
            Modal: 'exports-loader?Modal!bootstrap/js/dist/modal',
            Popover: 'exports-loader?Popover!bootstrap/js/dist/popover',
            Scrollspy: 'exports-loader?Scrollspy!bootstrap/js/dist/scrollspy',
            Tab: 'exports-loader?Tab!bootstrap/js/dist/tab',
            Tooltip: "exports-loader?Tooltip!bootstrap/js/dist/tooltip",
            Util: 'exports-loader?Util!bootstrap/js/dist/util'
        })
    ],
    output:{
        filename:'bundle.js',
        path: path.resolve(__dirname, 'dist')
    },
    optimization:{
        splitChunks:{
            chunks: 'all'
        }
    },
    module:{
        rules:[
            {
                test: /\.(png|jpg|gif)$/,
                use:[ 'file-loader' ]
            },
            {
                test: /\.html$/,
                use:[ { loader:'html-loader', options:{minimize:true} } ]
            },
            {
                test: /\.js$/,
                loader: 'babel-loader',
                exclude: /(node_modules|bower_components)/,
                query:{presets:['@babel/preset-env']}
            },
            {
                test: /\.css$/,
                include: /node_modules/,
                use: [
                    {loader: 'style-loader'}, 
                    {loader:'css-loader'},
                ]
            },
            {
                test: /\.scss$/,
                include: /sass/,
                use: ['style-loader', 'css-loader', 'sass-loader']
            },
            {
                test: /\.woff2?$|\.ttf$|\.eot$|\.svg$/,
                use:[{loader:'file-loader'}]
            }
        ]
    },
    resolve:{
        alias:{
            'node_modules': path.resolve(__dirname, 'node_modules'),
            'jquery': require.resolve('jquery'),
            'smartresize': path.resolve(__dirname, 'src/js/helpers/smartresize.js')
        }
    }
};
